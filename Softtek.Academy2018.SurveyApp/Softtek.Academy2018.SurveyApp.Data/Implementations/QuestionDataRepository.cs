﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Data.Entity;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class QuestionDataRepository : IQuestionRepository
    {
        public int Add(Question survey)
        {
            using (var context = new SuerveyDbContext())
            {
                context.Questions.Add(survey);

                context.SaveChanges();

                return survey.Id;
            }
        }

        public bool AddOption(Question question, Option option)
        {
            using (var context = new SuerveyDbContext())
            {
                var currentQuestion = context.Questions.SingleOrDefault(x => x.Id == question.Id);

                if (currentQuestion == null) return false;

                currentQuestion.Options.Add(option);

                context.SaveChanges();
            
                return true;
            }
        }

        public bool Delete(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                Question currentQuestion = context.Questions.SingleOrDefault(x => x.Id == id);

                if (currentQuestion == null) return false;

                currentQuestion.IsActive = false;
                currentQuestion.ModifiedDate = DateTime.Now;


                context.Entry(currentQuestion).State = EntityState.Modified;

                context.SaveChanges();

                return true;
            }
        }

        public Question Get(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Questions.AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public bool Update(Question survey)
        {
            using (var context = new SuerveyDbContext())
            {
                Question currentQuestion = context.Questions.SingleOrDefault(x => x.Id == survey.Id);

                if (currentQuestion == null) return false;

                currentQuestion.CreatedDate = survey.CreatedDate;
                currentQuestion.ModifiedDate = DateTime.Now;
                currentQuestion.Text = survey.Text;

                context.Entry(currentQuestion).State = EntityState.Modified;

                context.SaveChanges();

                return true;
            }
        }
    }
}
