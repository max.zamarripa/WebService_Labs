﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Data.Entity;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class OptionDataRepository : IOptionRepository
    {
        public int Add(Option survey)
        {
            using (var context = new SuerveyDbContext())
            {
                context.Options.Add(survey);
                context.SaveChanges();
                return survey.Id;
            }
        }

        public bool Delete(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                var result = context.Options.SingleOrDefault(x => x.Id == id);
                if (result == null) return false;

                context.Options.Remove(result);
                
                context.SaveChanges();
                return true;
            }
        }

        public bool Exist(string Text)
        {
            using (var context = new SuerveyDbContext())
            {
               return context.Options.AsNoTracking().Any(x => x.Text.ToLower() == Text.ToLower());
            }
        }

        public Option Get(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Options.AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public bool Update(Option survey)
        {
            using (var context = new SuerveyDbContext())
            {
                Option currentOption = context.Options.SingleOrDefault(x => x.Id == survey.Id);

                if (currentOption == null) return false;

                currentOption.CreatedDate = survey.CreatedDate;
                currentOption.ModifiedDate = DateTime.Now;
                currentOption.Text = survey.Text;

                context.Entry(currentOption).State = EntityState.Modified;

                context.SaveChanges();

                return true;
            }
        }
    }
}
