﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Data.Entity;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class SurveyDataRepository : ISurveyRepository
    {
        public int Add(Survey survey)
        {
            using (var context = new SuerveyDbContext())
            {
                context.Surveys.Add(survey);
                context.SaveChanges();
                return survey.Id;
            }
        }

        public bool Delete(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                Survey currentSurvey = context.Surveys.SingleOrDefault(x => x.Id == id);

                if (currentSurvey == null) return false;

                currentSurvey.IsArchived = false;
                currentSurvey.ModifiedDate = DateTime.Now;


                context.Entry(currentSurvey).State = EntityState.Modified;

                context.SaveChanges();

                return true;
            }
        }

        public Survey Get(int id)
        {
            using (var context = new SuerveyDbContext())
            {
                return context.Surveys.AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public bool Update(Survey survey)
        {
            using (var context = new SuerveyDbContext())
            {
                Survey currentSurvey = context.Surveys.SingleOrDefault(x => x.Id == survey.Id);

                if (currentSurvey == null) return false;

                currentSurvey.CreatedDate = survey.CreatedDate;
                currentSurvey.ModifiedDate = DateTime.Now;
                currentSurvey.Description = survey.Description;
                currentSurvey.Title = survey.Title;
                currentSurvey.IsArchived = survey.IsArchived;

                context.Entry(currentSurvey).State = EntityState.Modified;

                context.SaveChanges();

                return true;
            }
        }
    }
}
