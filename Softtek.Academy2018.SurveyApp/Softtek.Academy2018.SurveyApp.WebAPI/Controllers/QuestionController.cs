﻿using Softtek.Academy2018.SurveyApp.Bussin.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Controllers
{
    [RoutePrefix("api/question")]
    public class QuestionController : ApiController
    {
        private readonly IQuestionService _questionService;

        public QuestionController(IQuestionService questionService)
        {
            _questionService = questionService;
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] QuestionDTO questionDTO)
        {
            if (questionDTO == null) return BadRequest("Request is null");

            Question question = new Question
            {
                Text = questionDTO.Text,
                QuestionTypeId = questionDTO.QuestionTypeId,
                IsActive = false,
                CreatedDate = DateTime.Now
            };

            int id = _questionService.Add(question);

            if (id <= 0) return BadRequest("Unable to create question");

            var payload = new { questionId = id };

            return Ok(payload);
        }

        [Route("{id:int}")]
        [HttpDelete]
        public IHttpActionResult Delete([FromUri] int id)
        {
            var result = _questionService.Delete(id);

            if (!result) return BadRequest("Unable to delete question");

            return Ok();
        }

        [Route("{id:int}")]
        [HttpGet]
        public IHttpActionResult GetById([FromUri] int id)
        {
            Question question = _questionService.Get(id);

            if (question == null) return NotFound();

            QuestionDTO questionDTO = new QuestionDTO
            {
                Id = question.Id,
                Text = question.Text,
                IsActive = question.IsActive,
                QuestionTypeId = question.QuestionTypeId,
                CreatedDate = question.CreatedDate,
                ModifiedDate = question.ModifiedDate
            };

            return Ok(questionDTO);

        }
    }
}
