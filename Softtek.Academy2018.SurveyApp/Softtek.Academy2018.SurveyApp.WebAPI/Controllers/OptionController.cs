﻿using Softtek.Academy2018.SurveyApp.Bussin.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Controllers
{
    [RoutePrefix("api/option")]
    public class OptionController : ApiController
    {
        private readonly IOptionService _optionService;

        public OptionController(IOptionService optionService)
        {
            _optionService = optionService;
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] OptionDTO optionDTO)
        {
            if (optionDTO == null) return BadRequest("Request is null");

            Option option = new Option
            {
                Text = optionDTO.Text,
                CreatedDate = DateTime.Now
            };

            int id = _optionService.Add(option);

            if (id <= 0) return BadRequest("Unable to create option");

            var payload = new { optionId = id };

            return Ok(payload);
        }

        [Route("{id:int}")]
        [HttpDelete]
        public IHttpActionResult Delete([FromUri] int id)
        {
            var result = _optionService.Delete(id);

            if (!result) return BadRequest("Unable to delete option");

            return Ok();
        }


        [Route("{id:int}")]
        [HttpGet]
        public IHttpActionResult GetById([FromUri] int id)
        {
            Option option = _optionService.Get(id);

            if (option == null) return NotFound();

            OptionDTO optionDTO = new OptionDTO
            {
                Id = option.Id,
                Text = option.Text,
                CreatedDate = option.CreatedDate,
                ModifiedDate = option.ModifiedDate
            };

            return Ok(optionDTO);

        }
    }
}
