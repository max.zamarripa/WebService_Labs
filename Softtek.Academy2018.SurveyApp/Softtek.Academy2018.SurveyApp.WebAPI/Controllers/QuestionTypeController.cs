﻿using Softtek.Academy2018.SurveyApp.Bussin.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Controllers
{
    [RoutePrefix("api/questiontype")]
    public class QuestionTypeController : ApiController
    {
        private readonly IQuestionTypeService _questionTypeService;

        public QuestionTypeController(IQuestionTypeService questionTypeService)
        {
            _questionTypeService = questionTypeService;
        }

        [Route("{id:int}")]
        [HttpGet]
        public IHttpActionResult GetById([FromUri] int id)
        {
            QuestionType questionType = _questionTypeService.Get(id);

            if (questionType == null) return NotFound();

            QuestionTypeDTO questionTypeDTO = new QuestionTypeDTO
            {
                Id = questionType.Id,
                Description = questionType.Description,
                CreatedDate = questionType.CreatedDate,
                ModifiedDate = questionType.ModifiedDate
            };

            return Ok(questionTypeDTO);

        }

        [Route("~/api/questiontypes")]
        [HttpGet]
        public IHttpActionResult GetALL()
        {
            ICollection<QuestionType> questiontypes = _questionTypeService.GetAll();

            if (questiontypes == null) return NotFound();

            ICollection<QuestionTypeDTO> questionTypeList = questiontypes.Select(p => new QuestionTypeDTO
            {
                Id = p.Id,
                Description = p.Description,
                CreatedDate = p.CreatedDate,
                ModifiedDate = p.ModifiedDate
            }).ToList();

            return Ok(questionTypeList);
        }
    }
}
