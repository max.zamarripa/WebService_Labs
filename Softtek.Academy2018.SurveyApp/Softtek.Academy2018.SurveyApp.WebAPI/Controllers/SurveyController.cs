﻿using Softtek.Academy2018.SurveyApp.Bussin.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Controllers
{
    [RoutePrefix("api/survey")]
    public class SurveyController : ApiController
    {
        private readonly ISurveyService _surveyService;

        public SurveyController(ISurveyService surveyService)
        {
            _surveyService = surveyService;
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] SurveyDTO surveyDTO)
        {
            if (surveyDTO == null) return BadRequest("Request is null");

            Survey survey = new Survey
            {
                Title = surveyDTO.Title,
                Description = surveyDTO.Description,
                IsArchived = false,
                CreatedDate = DateTime.Now
            };

            int id = _surveyService.Add(survey);

            if (id <= 0) return BadRequest("Unable to create survey");

            var payload = new { surveyId = id };

            return Ok(payload);
        }

        [Route("{id:int}")]
        [HttpDelete]
        public IHttpActionResult Delete([FromUri] int id)
        {
            var result = _surveyService.Delete(id);

            if (!result) return BadRequest("Unable to delete survey");

            return Ok();
        }

        [Route("{id:int}")]
        [HttpGet]
        public IHttpActionResult GetById([FromUri] int id)
        {
            Survey survey = _surveyService.Get(id);

            if (survey == null) return NotFound();

            SurveyDTO surveyDTO = new SurveyDTO
            {
                Id = survey.Id,
                Description = survey.Description,
                IsArchived = survey.IsArchived,
                Title = survey.Title,
                CreatedDate = survey.CreatedDate,
                ModifiedDate = survey.ModifiedDate
            };

            return Ok(surveyDTO);

        }
    }
}
