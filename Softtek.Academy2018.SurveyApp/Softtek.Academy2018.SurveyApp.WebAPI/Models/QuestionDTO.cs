﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Models
{
    public class QuestionDTO
    {
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string Text { get; set; }

        public bool IsActive { get; set; }

        public int QuestionTypeId { get; set; }
    }
}