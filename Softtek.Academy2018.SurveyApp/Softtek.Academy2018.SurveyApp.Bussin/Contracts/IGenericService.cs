﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Bussin.Contracts
{
    public interface IGenericService<T> where T : class
    {
        int Add(T survey);

        T Get(int id);

        bool Update(T survey);

        bool Delete(int id);
    }
}
