﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Bussin.Contracts
{
    public interface IQuestionService : IGenericService<Question>
    {
        bool AddOption(Question question, Option option);
    }
}
