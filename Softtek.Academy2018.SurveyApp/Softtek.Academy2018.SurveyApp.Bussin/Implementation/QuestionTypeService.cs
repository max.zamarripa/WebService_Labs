﻿using Softtek.Academy2018.SurveyApp.Bussin.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.Data.Contracts;

namespace Softtek.Academy2018.SurveyApp.Bussin.Implementation
{
    public class QuestionTypeService : IQuestionTypeService
    {

        private readonly IQuestionTypeRepository _dataRepo;

        public QuestionTypeService(IQuestionTypeRepository dataRepo)
        {
            _dataRepo = dataRepo;
        }

        public QuestionType Get(int id)
        {
            return _dataRepo.Get(id);
        }

        public ICollection<QuestionType> GetAll()
        {
            return _dataRepo.GetAll();
        }
    }
}
 