﻿using Softtek.Academy2018.SurveyApp.Bussin.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Bussin.Implementation
{
    public class QuestionService :  IQuestionService
    {
        private readonly IQuestionRepository _dataRepo;

        public QuestionService(IQuestionRepository dataRepo)
        {
            _dataRepo = dataRepo;
        }

        public int Add(Question survey)
        {
            return _dataRepo.Add(survey);
        }

        public bool AddOption(Question question, Option option)
        {

            if(question.Options.Count() >= 3)
            {
                question.IsActive = true;
            }

            return _dataRepo.AddOption(question, option);
        }

        public bool Delete(int id)
        {
            _dataRepo.Delete(id);
            return true;
        }

        public Question Get(int id)
        {
            return _dataRepo.Get(id);
        }

        public bool Update(Question survey)
        {
            return _dataRepo.Update(survey);
        }
    }
}
