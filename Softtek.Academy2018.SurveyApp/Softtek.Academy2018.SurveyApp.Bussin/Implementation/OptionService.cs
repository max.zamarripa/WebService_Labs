﻿using Softtek.Academy2018.SurveyApp.Bussin.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.Data.Contracts;

namespace Softtek.Academy2018.SurveyApp.Bussin.Implementation
{
    public class OptionService : IOptionService
    {
        private readonly IOptionRepository _dataRepo;

        public OptionService(IOptionRepository dataRepo)
        {
            _dataRepo = dataRepo;
        }

        public int Add(Option survey)
        {
            //Una opción no puede repetirse dentro de la tabla de opciones
            var Exist = _dataRepo.Exist(survey.Text);
            if (Exist) return 0;

            return _dataRepo.Add(survey);
        }

        public bool Delete(int id)
        {
            var search = _dataRepo.Get(id);
            if (search == null) return false;
            
            _dataRepo.Delete(id);
            return true;
        }

        public Option Get(int id)
        {
            return _dataRepo.Get(id);
        }

        public bool Update(Option survey)
        {
            return _dataRepo.Update(survey);
        }
    }
}
