﻿using Softtek.Academy2018.SurveyApp.Bussin.Contracts;
using Softtek.Academy2018.SurveyApp.Data.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Bussin.Implementation
{
    public class SurveyService : ISurveyService
    {
        private readonly ISurveyRepository _dataRepo;

        public SurveyService(ISurveyRepository dataRepo)
        {
            _dataRepo = dataRepo;
        }

        public int Add(Survey survey)
        {
            return _dataRepo.Add(survey);
        }

        public bool Delete(int id)
        {
            _dataRepo.Delete(id);
            return true;
        }

        public Survey Get(int id)
        {
            return _dataRepo.Get(id);
        }

        public bool Update(Survey survey)
        {
            return _dataRepo.Update(survey);
        }
    }
}
